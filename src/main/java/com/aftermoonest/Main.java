package com.aftermoonest;

import com.aftermoonest.model.domain.plant.Flowerpot;
import com.aftermoonest.model.domain.plant.Plant;
import com.aftermoonest.view.View;

public class Main {
    public static void main(String[] args) {
        new View().show();
    }
}
