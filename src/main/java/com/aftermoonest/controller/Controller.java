package com.aftermoonest.controller;

import com.aftermoonest.model.domain.plant.Plant;

import java.util.List;

public interface Controller {
    StringBuilder getPlants();

    StringBuilder getBouquet();

    void generateDefaultList();

    void addFlower(String latinName, String kind, String colorOfFlowering, boolean isGardenPlant, int condition);

    void addFlowerpot(String latinName, String kind, String colorOfFlowering, double diameterOfPot, int age);

    List<Plant> createBouquet(int countOfFlowers);
}
