package com.aftermoonest.controller;

import com.aftermoonest.model.BusinessLogic;
import com.aftermoonest.model.Model;
import com.aftermoonest.model.domain.plant.Plant;

import java.util.List;

public class ControllerImpl implements Controller {
    Model model;

    public ControllerImpl(){
        model = new BusinessLogic();
    }

    @Override
    public StringBuilder getPlants() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < model.getPlants().size(); i++) {
            builder.append(model.getPlants().get(i));
            builder.append("\n");
        }
        return builder;
    }

    @Override
    public StringBuilder getBouquet(){
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < model.getBouquet().size(); i++) {
            builder.append(model.getBouquet().get(i));
            builder.append("\n");
        }
        return builder;
    }

    @Override
    public void generateDefaultList() {
        model.generateDefaultList();
    }

    @Override
    public void addFlower(String latinName, String kind, String colorOfFlowering, boolean isGardenPlant, int condition) {
        model.addFlower(latinName, kind, colorOfFlowering, isGardenPlant, condition);
    }

    @Override
    public void addFlowerpot(String latinName, String kind, String colorOfFlowering, double diameterOfPot, int age) {
        //model.addFlowerpot();
    }

    @Override
    public List<Plant> createBouquet(int countOfFlowers) {
        return model.createBouquet(countOfFlowers);
    }
}
