package com.aftermoonest.model;

import com.aftermoonest.model.domain.plant.Flower;
import com.aftermoonest.model.domain.plant.Flowerpot;
import com.aftermoonest.model.domain.plant.Plant;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BusinessLogic implements Model {

    private List<Plant> plants;

    private List<Plant> bouquet;

    public BusinessLogic() {
        plants = new ArrayList<>();
    }

    @Override
    public List<Plant> getPlants() {
        return plants;
    }

    @Override
    public List<Plant> getBouquet() {
        return bouquet;
    }


    @Override
    public void generateDefaultList() {
        plants.add(new Flower("Rose", "Felicite Parmentier", "RED", true, 10));
        plants.add(new Flower("Violets", "Viola palustris", "BLUE", false, 2));
        plants.add(new Flower("Chamomile", "Matricaria chamomilla", "WHITE", true, 6));
        plants.add(new Flower("Peonies", "Paeonia algeriensis", "ROSE", true, 6));
        plants.add(new Flowerpot("Cactus", "Gymnocalycium mihanovichii", "GREEN", 2.5, 2));
        plants.add(new Flowerpot("Orchid", "Vanilla planifolia", "YELLOW", 2.5, 3));
        plants.add(new Flowerpot("Ficus", "Ficus benjamina", "GREEN", 2.5, 4));
    }

    @Override
    public void addFlower(String latinName, String kind, String colorOfFlowering, boolean isGardenPlant, int condition) {
        Plant plant = new Flower(latinName, kind, colorOfFlowering, isGardenPlant, condition);
        plants.add(plant);
    }

    @Override
    public void addFlowerpot(String latinName, String kind, String colorOfFlowering, double diameterOfPot, int age) {
        Plant plant = new Flowerpot(latinName, kind, colorOfFlowering, diameterOfPot, age);
        plants.add(plant);

    }

    @Override
    public List<Plant> createBouquet(int countOfFlowers) {
        bouquet = new ArrayList<>();
        for (int i = 0; i < countOfFlowers; i++) {
            if (!(plants.get(i) instanceof Flowerpot)) {
                bouquet.add(plants.get(i));
            }
        }
        bouquet.sort(Comparator.comparing(Plant::getPrice));
        return bouquet;
    }
}
