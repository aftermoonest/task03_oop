package com.aftermoonest.model;

import com.aftermoonest.model.domain.plant.Plant;

import java.awt.*;
import java.util.List;

public interface Model {
    List<Plant> getPlants();

    List<Plant> getBouquet();

    void generateDefaultList();

    void addFlower(String latinName, String kind, String colorOfFlowering, boolean isGardenPlant, int condition);

    void addFlowerpot(String latinName, String kind, String colorOfFlowering, double diameterOfPot, int age);

    List<Plant> createBouquet(int countOfFlowers);
}
