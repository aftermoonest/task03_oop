package com.aftermoonest.model.domain.plant;

import java.awt.*;

public class Flower extends Plant {
    private boolean isGardenPlant;
    private int condition;

    public Flower(String latinName, String kind, String colorOfFlowering, boolean isGardenPlant, int condition) {
        super(latinName, kind, colorOfFlowering);
        this.isGardenPlant = isGardenPlant;
        this.condition = condition;
        countPrice();
    }

    void countPrice() {
        if (isGardenPlant){
            price = condition * 10;
        }
        else {
            price = condition * 5;
        }
    }

    @Override
        public String toString() {
            return "Flower{" +
                    "isGardenPlant=" + isGardenPlant +
                    ", latinName='" + latinName + '\'' +
                    ", kind='" + kind + '\'' +
                    ", price=" + price +
                    ", condition=" + condition +
                    ", colorOfFlowering=" + colorOfFlowering +
                    '}';
    }
}
