package com.aftermoonest.model.domain.plant;

import java.awt.*;

public class Flowerpot extends Plant {
    private double diameterOfPot;
    private int age;

    public Flowerpot(String latinName, String kind, String colorOfFlowering, double diameterOfPot, int age) {
        super(latinName, kind, colorOfFlowering);
        this.diameterOfPot = diameterOfPot;
        this.age = age;
        countPrice();
    }

    void countPrice() {
        price = ((age + diameterOfPot) * 10);
    }

    @Override
    public String toString() {
        return "Flowerpot{" +
                "diameterOfPot=" + diameterOfPot +
                ", latinName='" + latinName + '\'' +
                ", kind='" + kind + '\'' +
                ", price=" + price +
                ", age=" + age +
                ", colorOfFlowering=" + colorOfFlowering +
                '}';
    }
}
