package com.aftermoonest.model.domain.plant;

import java.awt.*;
import java.util.Comparator;
import java.util.Objects;

public abstract class Plant implements Comparator<Plant> {

    double price;

    public double getPrice() {
        return price;
    }

    String latinName;

    String kind;
    String colorOfFlowering;

    Plant(String latinName, String kind, String colorOfFlowering) {
        this.latinName = latinName;
        this.kind = kind;
        this.colorOfFlowering = colorOfFlowering;
    }

    abstract void countPrice();

    @Override
    public String toString() {
        return "Plant{" +
                "price=" + price +
                ", latinName='" + latinName + '\'' +
                ", kind='" + kind + '\'' +
                ", colorOfFlowering=" + colorOfFlowering +
                '}';
    }

    @Override
    public int compare(Plant o1, Plant o2) {
        return Double.compare(o1.price, o2.price);
    }
}
