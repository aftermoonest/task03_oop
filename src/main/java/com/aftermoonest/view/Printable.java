package com.aftermoonest.view;

@FunctionalInterface
public interface Printable {
    void print();
}
