package com.aftermoonest.view;

import com.aftermoonest.controller.Controller;
import com.aftermoonest.controller.ControllerImpl;
import com.aftermoonest.model.domain.plant.Plant;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print list");
        menu.put("2", "2 - Generate default list");
        menu.put("3", "3 - Add plant");
        menu.put("4", "4 - Create bouquet");
        menu.put("Q", "Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printListButton);
        methodsMenu.put("2", this::generateDefaultListButton);
        methodsMenu.put("3", this::addPlantButton);
        methodsMenu.put("4", this::createBouquetButton);
    }

    private void printListButton() {
        System.out.println(controller.getPlants());
    }

    private void generateDefaultListButton() {
        controller.generateDefaultList();
        System.out.println(controller.getPlants());
    }

    private void addPlantButton() {
        String key = "";
        System.out.println("1 - Flower\n" +
                "2 - Flowerpot");
        System.out.print("What is it must to be: ");
        key = input.nextLine().toUpperCase();
        if (key.equals("1")) {
            enterFlower();
        } else if (key.equals("2")) {
            enterFlowerpot();
        } else {
            System.err.println("Wrong!");
        }
    }

    private void enterFlower() {
        System.out.print("Enter latin name: ");
        String latinName = input.nextLine();

        System.out.print("Enter kind: ");
        String kind = input.nextLine();

        System.out.print("Enter color: ");
        String colorOfFlowering = input.nextLine();

        System.out.print("Enter is garden plant (true or false): ");
        boolean isGardenPlant = input.nextBoolean();

        System.out.print("Enter condition: ");
        int condition = input.nextInt();

        controller.addFlower(latinName, kind, colorOfFlowering, isGardenPlant, condition);
    }

    private void enterFlowerpot() {
        System.out.print("Enter latin name: ");
        String latinName = input.nextLine();

        System.out.print("Enter kind: ");
        String kind = input.nextLine();

        System.out.print("Enter color: ");
        String colorOfFlowering = input.nextLine();

        System.out.print("Enter diameter: ");
        double diameter = input.nextDouble();

        System.out.print("Enter age: ");
        int age = input.nextInt();

        controller.addFlowerpot(latinName, kind, colorOfFlowering, diameter, age);
    }

    private void createBouquetButton() {
        System.out.print("Enter count of flowers in bouquet: ");
        int count = input.nextInt();
        controller.createBouquet(count);
        System.out.println(controller.getBouquet());
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String key;
        do {
            outputMenu();
            System.out.print("Enter: ");
            key = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(key).print();
            } catch (Exception e) {

            }
        } while (!key.equals("Q"));
    }
}
